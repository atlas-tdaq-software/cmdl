
tdaq_package()

tdaq_add_library(cmdline src/lib/*.cpp  DEFINITIONS PRIVATE -Dunix_style)

tdaq_add_executable(cmdparse 
  src/cmd/*.cpp
  INCLUDE_DIRECTORIES PRIVATE src/lib
  LINK_LIBRARIES cmdline
  DEFINITIONS -Dunix_style)

tdaq_add_executable(cmdtest tests/cmdtest.cc LINK_LIBRARIES cmdline)
tdaq_add_executable(cmdl_test NOINSTALL tests/daqtest.cc LINK_LIBRARIES cmdline)
