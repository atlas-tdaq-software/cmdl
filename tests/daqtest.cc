//------------------------------------------------------------------------
// ^FILE: daqtest.c - test program for the CmdLine library
//
// ^DESCRIPTION:
//    This program tests as many features of command-line as possible.
//	Based on Brad Appleton's cmdtest program.
//
// ^HISTORY:
//    08/01/98	Bob Jones		Created
//-^^---------------------------------------------------------------------

#include <cstdlib>
#include <iostream>
#include <ctype.h>
#include <cmdl/cmdargs.h>

using namespace std ;

//------------------------------------------------------------------------ main

int
main(int argc,  char * argv[]) {

   CmdArgStr       partition('p', "partition", "partition", "partition name", CmdArg::isREQ);
   CmdArgInt       debug ('D', "Debug", "[level]", "turn on debugging",
                              CmdArg::isVALSTICKY);
   CmdArgStr       server('S', "server", "server", "server name");
   CmdArgStrList   datafiles('d',"datafiles", "[datafiles ...]", "data files for database");

   CmdLine  cmd(*argv,
                & partition,
                & debug,
                & server,
                & datafiles,
                NULL);

   CmdArgvIter  argv_iter(--argc, (const char * const *) ++argv);

   cmd.description(
"This program is intended to give an example of how to use the \
the CmdLine(3C++) class library in the DAQ."
   );

   cout << "Test of " << CmdLine::ident() << endl ;

   debug = 0;

// @@ Igor Soloviev: Bob, never use NULL in C++ !
   partition = 0;
   server = 0;

   cout << "Parsing the command-line ..." << endl ;
   unsigned status = cmd.parse(argv_iter);
   if (status)  cmd.error() << "parsing errors occurred!" << endl ;

   unsigned dbg_flags = debug.flags();
   if ((dbg_flags & CmdArg::GIVEN) && (! (dbg_flags & CmdArg::VALGIVEN))) {
      debug = 1;
   }
   cout << "debug:" << debug << endl;

   // Parse partition
   if (! partition.isNULL())
      cout << "Partition:" << partition << endl ;
   else
   	cout << "Partition not defined" << endl;


   // Parse server
   if (! server.isNULL())
      cout << "server:" << server << endl ;
   else
   	cout << "server not defined" << endl;

	// parse data files
   unsigned df_flags = datafiles.flags();
	if ((df_flags & CmdArg::GIVEN)){
		for (size_t i=0; i<datafiles.count();i++) {
			cout << "datafile[" << i 
				<< "]=" << datafiles[i] << endl;
		}
	} else cout << "datafiles not defined" << endl;
   return  0;
}

